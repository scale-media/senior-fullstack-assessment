<?php

namespace Scale;

/**
 * Node represents a node in a tree
 */
class Node
{
	/**
	 * The Node's ID
	 *
	 * @var integer
	 */
	public $id;

	/**
	 * Child nodes
	 *
	 * @var Node[]
	 */
	public $children = [];

	/**
	 * Create a Node with an id
	 *
	 * @param integer $id
	 */
	public function __construct($id)
	{
		$this->id = $id;
	}
}
