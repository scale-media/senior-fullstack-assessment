<?php

/**
 * This is a test script for Exercise #2 in which you should:
 *
 * 1. Create a MyTreeStorage class which implements Scale\TreeStorageInterface
 * 2. Get the tests in this file to pass
 *
 * Note: this test should save and load files from the project's "data" directory
 */

require(dirname(__FILE__) . "/../src/autoload.php");

// this is the full path to the file to save/load
$path = dirname(__FILE__) . "/../data/my.tree";

$treeToSave = (new Scale\SampleTree)->create();

$storage = new \MyTreeStorage();

$storage->save($path, $treeToSave);
$loaded = $storage->load($path);

echo "\r\nExercise #2 - save() and load():\r\n";

if (is_object($loaded) &&
	get_class($loaded) == "Scale\Node" &&
	$treeToSave->id == $loaded->id
) {
	echo "passed\r\n";
} else {
	echo "failed\r\n";
}
