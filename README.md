# ScaleMedia - Senior Full-stack Engineer Code Assessment

## Overview

In this assessment you will be working with a tree structure with the following assumptions:

* A node in a tree can have a variable number of child nodes
* Each node has a unique identifier

The following is an example of a tree structure:

![tree example](tree-example.png)

    // src/Scale/Node.php

    /**
     * Node represents a node in a tree
     */
    class Node
    {
      /**
       * The Node's ID
       *
       * @var integer
       */
      public $id;

      /**
       * Child nodes
       *
       * @var Node[]
       */
      public $children = [];
    }

## Exercises

### Exercise #1 (test/Exercise1.php)

In this exercise you will create a class named MyTreeHandler which
implements the two methods from the Scale\TreeHandlerInterface in order for
the tests in Exercise1 to pass.

Your methods should be able to be called repeatedly on a class instance with different input values and return the correct results.

### Exercise #2 (test/Exercise2.php)

In this exercise you will create a class named MyTreeStorage which implements the two methods from the Scale\TreeStorageInterface in order for the tests in Exercise2 to pass.

The method and format of serializing/deserializing the tree is left up to you.

### Exercise #3 (web application)

Build a single page web application which is configured to be launched in a Docker container and fulfills the following requirements:

1. Has a single REST API endpoint to retrieve a tree by ID. This endpoint should use your storage class and returns it as a JSON structure.

    For example:

        GET http://localhost:8000/api/tree/123

        {
            "id": 1,
            "children": [
                {
                    "id": 2,
                    ...
                },
                {
                    "id": 10,
                    ...
                },
                ...
            ]
        }

    Note: the ID from the endpoint should map to a tree located in a file for your MyTreeStorage class to load. For example: /api/tree/100 = data/100.tree

2. Has a single page web app which loads a tree by id from your REST API and renders a visual representation of the tree (similar to the visualization in tree-example.png)

* You are free to use a Javascript library to render the tree visualization
* You can hardcode the endpoint URL in your Javascript to always load a single tree ID for demonstration purposes

## Deliverables

In order to submit your solution:

1. Clone this repository.

2. Add your solutions.

3. Add instructions to INSTRUCTIONS.md on how to run your application in Docker.

4. Include a sample serialized tree file in data/ which your storage class will load in order for the web application to run correctly.

5. Compress this directory. (should be less than 20MB in final size)

6. Email the compressed file to your recruiter.

It is expected that we can uncompress the directory, run the two test files via CLI with passing results, and launch the web application in Docker without modifying any code.
